#!/bin/bash

sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg2 \
        software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key  add -

ARCH="(`dpkg --print-architecture`)"
if ["${ARCH})" = "amd64"] || ["${ARCH}" = "x86_64"]; then
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/debian \
          \$(lsb_release -cs) \
             stable"

else
    if ["${ARCH})" = "arm64"]; then
        sudo add-apt-repository \
               "deb [arch=arm64] https://download.docker.com/linux/debian \
                  \$(lsb_release -cs) \
                     stable"
    fi
fi

sudo apt-get update
sudo apt-get install docker-ce
echo "(`docker --version`)"


class DecksController < ApplicationController

    before_action :authenticate_user!
    
    def index
        @decks= current_user.decks.all
        json_response(@decks)
    end
    def create
        @deck = current_user.decks.create(deck_params)
        if @deck.save
            json_response(@deck)
        else
            json_response(@deck.errors) #tratar erro adequadamente
        end
    end
    def update 
        @deck = current_user.decks.find(params[:id])
        if @deck.update(deck_params)
            json_response @deck
        else
            json_reponse @deck.errors #tratar erros
        end
    end

    def destroy
        @deck = Deck.find(params[:id])
        @deck.destroy
        #redirect_to decks_path o que fazer aqui?
    end

    def show
        print "################################ param #{params}"
        @deck = Deck.find(params[:id])
        json_response(@deck)
    end
   
    def next_card
        @deck = Deck.find(params[:id])
        print "################################ param #{params}"
        @card = @deck.cards.order("RANDOM()").first #montar funcao de verdade no modelo
        json_response @card
    end
    private

    def deck_params
        params.permit(:name,:description)
    end
end

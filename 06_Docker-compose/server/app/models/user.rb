class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: JwtBlacklist
  
  validates :name, presence: true, length: {minimum: 5}
  #validates :email, presence: true, uniqueness: true
  
  has_many :decks, dependent: :destroy
end

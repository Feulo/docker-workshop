Rails.application.routes.draw do
    devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'user/sessions',
               registrations: 'user/registrations'
             },
             defaults: {format: 'json'}
        
    resources :decks, defaults:{format: 'json'}  do
        resources :cards
    end
    get 'decks/:id/next_card', to: 'decks#next_card', defaults:{format: 'json'}
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

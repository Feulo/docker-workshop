class DecksController < ApplicationController

    before_action :authenticate_user!
    before_action :set_deck, only: [:show, :update, :destroy, :next_card]
    def index
        @decks= current_user.decks.all
        json_response(@decks)
    end
    
    def show
        json_response(@deck)
    end
    
    def create
        #@deck = current_user.decks.create(deck_params)
        @deck = current_user.decks.create!(deck_params)
        json_response(@deck, :created)
        #if @deck.save
        #    json_response(@deck)
        #else
        #    json_response(@deck.errors) #tratar erro adequadamente
        #end
    end
    def update 
        #@deck = current_user.decks.find(params[:id])
        #if @deck.update(deck_params)
        @deck.update!(deck_params)
        json_response(@deck, :ok)
        #    json_response @deck
        #else
        #    json_reponse @deck.errors #tratar erros
        #end
    end

    def destroy
        @deck.destroy
        head :no_content 
    end

   
    def next_card
        @card = @deck.cards.order("RANDOM()").first #montar funcao de verdade no modelo
        json_response @card
    end
    private

    def deck_params
        params.permit(:name,:description)
    end

    def set_deck
        @deck = Deck.find(params[:id])
    end
end

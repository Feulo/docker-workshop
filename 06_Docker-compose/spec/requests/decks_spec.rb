require 'rails_helper'

RSpec.describe 'Decks endpoint', type: :request do
    include Response
    let!(:user){create(:user)}
    let!(:decks) {create_list(:deck,3, user: user)}
    
    describe 'GET decks' do
        
        let(:url) {'/decks'}
        
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
     
        context 'if user is autheticated' do
            
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status ok' do      
                expect(response).to have_http_status(200) 
            end
            
            it 'should return a list of decks' do
                expect(json).not_to be_empty
                expect(json.size).to eq(3)
            end
            
            it ' returns all decks that belongs to user' do 
                expect(response.body).to eq(decks.to_json)
            end
        end
    end

    describe 'GET /decks/id' do
        
        let(:deck_id){decks.first.id}
        let(:url){"/decks/#{deck_id}"}
        
        context 'when user is unauthenticated' do
            
            before {get url}
            
            it 'returns  Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and record exists' do
            
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status ok' do      
                expect(response).to have_http_status(200) 
            end
            
            it 'should return a decks' do
                expect(json).not_to be_empty
                expect(json['id']).to eq(deck_id)
            end
            
            it ' returns all decks that belongs to user' do 
                expect(response.body).to eq(decks.first.to_json)
            end
        end
        
        context 'if user is autheticated and record does not exist' do
         
            let(:deck_id){1000}    
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status Not Found' do      
                expect(response).to have_http_status(404) 
            end
            
            it ' returns error message' do 
                expect(response.body).to match(/Couldn't find Deck/)
            end
        end
    end
   
    describe 'POST /decks' do
        let(:url){'/decks'} 
        let(:valid_attr){{name: 'deck_novo', description: 'info' }}
        
        context 'when user is unauthenticated' do
            
            before {post url, params: valid_attr}
            
            it 'returns  Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and deck is valid' do
            
            before{ 
                sign_in user
                post url, params: valid_attr
            }
            
            it 'returns status Created' do      
                expect(response).to have_http_status(201) 
            end
            
            it 'should return the new deck' do
                expect(json).not_to be_empty
                expect(json['name']).to eq 'deck_novo'
                expect(json['description']).to eq 'info'
            end
         
            it ' deck should belon to user' do 
                expect(json['user_id']).to eq(user.id)
            end
        end
      
        context 'if user is autheticated and deck is not valid' do
         
            let(:invalid_param){{name: 'acfd'}}    
            before{ 
                sign_in user
                post url, params: invalid_param
            }
            
            it 'returns Unprocessable entity' do      
                expect(response).to have_http_status(422) 
            end
            
            it ' returns error message' do
                expect(response.body).to match(/Validation failed:/)
            end
        end
    end 

    describe 'DELETE /decks/:id' do
        
        let(:deck_id){decks.first.id}
        let(:url){"/decks/#{deck_id}"}
        
        context 'when user is Unauthorized' do
            before {
                delete url
            }
            
            it 'returns Unathorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'does not delete the record' do
                expect(decks.first.id).to eq(deck_id)
            end
        end
        
        context 'when user is authorized' do
            
            before{ 
                sign_in user
                delete url
            }
            
            it 'returns No Content' do
                expect(response).to have_http_status(204)
            end
            
            it 'deletes the record' do
                expect{user.decks.find(deck_id)}.to raise_exception(ActiveRecord::RecordNotFound) 
            end
        end
    end

    describe 'PUT /decks/:id' do
        
        let(:deck_id){decks.first.id}
        let(:url){"/decks/#{deck_id}"} 
        let(:valid_attr){{description: 'info nova' }}
        
        context 'when user is unauthenticated' do
            
            before {put url, params: valid_attr}
            
            it 'returns  Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and change is valid' do
            
            before{ 
                sign_in user
                put url, params: valid_attr
            }
            
            it 'returns status ' do
                expect(response).to have_http_status(200) 
            end
            
            it 'should return the modificated deck' do
                expect(json).not_to be_empty
                expect(json['id']).to eq decks.first.id
                expect(json['name']).to eq decks.first.name
                expect(json['user_id']).to eq decks.first.user_id
                expect(json['description']).to eq 'info nova'
            end
        end
      
        context 'if user is autheticated and record does not exist' do
         
            let(:invalid_param){{name: 'acfd'}}    
            before{ 
                sign_in user
                put url, params: invalid_param
            }
            
            it 'returns Unprocessable entity' do      
                expect(response).to have_http_status(422) 
            end

            it 'record remain the same' do
                expect(user.decks.find(deck_id)['name']).not_to eq('acfd')
            end
        end     
    end
end

require 'rails_helper'

RSpec.describe Card, type: :model do
    
    context 'Associations' do
        it {should belong_to(:deck)}
    end

    context 'Validations' do
        it {should validate_presence_of(:front)}
        it {should validate_presence_of(:back)}
        it {should validate_uniqueness_of(:front).
            scoped_to(:deck_id).
            case_insensitive}
    end

end

